<?php

include_once 'common_ui.go';
$title = $title . " - Dashboard";
include_once 'views/header' . $URL_Extension;

$section = "";
if (isset($_GET['section']))
    $section = $_GET['section'];
if ($section == "")
    exit();
$election = "2015";


$dataGR = getSectionHistory($section, $election, $supportGR);
$dataSP = getSectionHistory($section, $election, $supportSP);
$dataAKD = getSectionHistory($section, $election, $supportAKD);
$dataOther = getSectionHistoryExcept($section, $election, array_merge($supportGR, $supportSP, $supportAKD));

$now = getCurrentResultForYear("2019", $section);

$c_GR = getResultName($name_GR, $now);
$c_SP = getResultName($name_SP, $now);
$c_AKD = getResultName($name_AKD, $now);
$c_Other = getResultName("Other", $now);

$nowNumbers = array($c_GR, $c_SP, $c_AKD, $c_Other);

//------------------
$data18GR = getSectionHistory($section, "2018", $supportGR);
$data18SP = getSectionHistory($section, "2018", $supportSP);
$data18AKD = getSectionHistory($section, "2018", $supportAKD);
$data18Other = getSectionHistoryExcept($section, "2018", array_merge($supportGR, $supportSP, $supportAKD));


//var_dump($data18Other);
$now18 = getCurrentResultForYear("2018", $section);

$c18_GR = getResultName($name_GR, $now18);
$c18_SP = getResultName($name_SP, $now18);
$c18_AKD = getResultName($name_AKD, $now18);
$c18_Other = getResultName("Other", $now18);

$old18Numbers = array($c18_GR, $c18_SP,  $c18_Other);
//---------


$OldNumbers = array($dataGR['VOTE'] == null ? 0 : $dataGR['VOTE'], $dataSP['VOTE'] == null ? 0 : $dataSP['VOTE'], $dataAKD['VOTE'] === null ? 0 : $dataAKD['VOTE'], $dataOther['VOTE']);
//var_dump($dataGR);

?>
<div class="row">
    <div id="app" class="col-sm-4">


        <div class="card card-body col-sm-12">
            <h2><?php echo $section; ?></h2>
        </div>
        <br>
        <div class="card card-body col-sm-12">


            <div class="col-sm-12  row border">
                <div class="col-sm-4"></div>
                <div class="col-sm-4 ">2019</div>
                <!--                <div class="col-sm-4 ">--><?php //echo $election; ?><!--</div>-->
            </div>
            <div class="col-sm-12  row">
                <div class="col-sm-4 "> GR</div>
                <div class="col-sm-4"><?php echo $c_GR; ?></div>
                <!--                <div class="col-sm-4">--><?php //echo $dataGR["VOTE"]; ?><!--</div>-->
            </div>
            <div class="col-sm-12  row">
                <div class="col-sm-4">SP</div>
                <div class="col-sm-4"><?php echo $c_SP; ?></div>
                <!--                <div class="col-sm-4">--><?php //echo $dataSP["VOTE"]; ?><!--</div>-->

            </div>
            <div class="col-sm-12  row">
                <div class="col-sm-4">AKD</div>
                <div class="col-sm-4"><?php echo $c_AKD; ?></div>
                <!--                <div class="col-sm-4">--><?php //echo $dataAKD["VOTE"]; ?><!--</div>-->

            </div>
            <div class="col-sm-12  row">
                <div class="col-sm-4">Other</div>
                <div class="col-sm-4"><?php echo $c_Other; ?></div>
                <!--                <div class="col-sm-4">--><?php //echo $dataOther["VOTE"]; ?><!--</div>-->

            </div>

        </div>
        <div class="card card-body col-sm-12">


            <div class="col-sm-12  row border">
                <div class="col-sm-4"></div>
                <!--                <div class="col-sm-4 ">2019</div>-->
                <div class="col-sm-4 "><?php echo $election; ?></div>
            </div>
            <div class="col-sm-12  row">
                <div class="col-sm-4 ">MR</div>
                <!--                <div class="col-sm-4">--><?php //echo $c_GR; ?><!--</div>-->
                <div class="col-sm-4"><?php echo $dataGR["VOTE"]; ?></div>
            </div>
            <div class="col-sm-12  row">
                <div class="col-sm-4">MS</div>
                <!--                <div class="col-sm-4">--><?php //echo $c_SP; ?><!--</div>-->
                <div class="col-sm-4"><?php echo $dataSP["VOTE"]; ?></div>

            </div>

            <div class="col-sm-12  row">
                <div class="col-sm-4">Other</div>
                <!--                <div class="col-sm-4">--><?php //echo $c_Other; ?><!--</div>-->
                <div class="col-sm-4"><?php echo $dataOther["VOTE"]; ?></div>

            </div>

        </div>
        <div class="card card-body col-sm-12">


            <div class="col-sm-12  row border">
                <div class="col-sm-4"></div>
                <!--                <div class="col-sm-4 ">2019</div>-->
                <div class="col-sm-4 "><?php echo "2018"; ?></div>
            </div>
            <div class="col-sm-12  row">
                <div class="col-sm-4 ">SLPP</div>
                <!--                <div class="col-sm-4">--><?php //echo $c_GR; ?><!--</div>-->
                <div class="col-sm-4"><?php echo $c18_GR["VOTE"]; ?></div>
            </div>
            <div class="col-sm-12  row">
                <div class="col-sm-4">UNP</div>
                <!--                <div class="col-sm-4">--><?php //echo $c_SP; ?><!--</div>-->
                <div class="col-sm-4"><?php echo $c18_SP["VOTE"]; ?></div>

            </div>

            <div class="col-sm-12  row">
                <div class="col-sm-4">Other</div>
                <!--                <div class="col-sm-4">--><?php //echo $c_Other; ?><!--</div>-->
                <div class="col-sm-4"><?php echo $data18Other["VOTE"]; ?></div>

            </div>

        </div>


    </div>
    <div id="app" class="col-sm-8 row">
        <div id="app" class="col-sm-6">
            <h2>2019</h2>


            <div class="card card-body col-sm-12">
                <div id="canvas-holder">
                    <canvas id="chart-area" height="200"></canvas>
                </div>
            </div>


        </div>
        <div id="app" class="col-sm-6">
            <h2><?php echo $election; ?></h2>


            <div class="card card-body col-sm-12">
                <div id="canvas-holder">
                    <canvas id="chart-area2" height="200"></canvas>
                </div>
            </div>


        </div>
        <div id="app" class="col-sm-6">
            <h2><?php echo "2018"; ?></h2>


            <div class="card card-body col-sm-12">
                <div id="canvas-holder">
                    <canvas id="chart-area3" height="200"></canvas>
                </div>
            </div>


        </div>
    </div>
</div>
<?php

include_once 'views/footer' . $URL_Extension;


?>
<script src="static/js/jquery.dataTables.min.js"></script>
<link href="static/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="static/css/datatable.bootstrap.css" rel="stylesheet">
<script src="static/js/Chart.js_2.7.3_Chart.min.js"></script>
<style>
    canvas {
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
    }
</style>
<script>
    window.chartColors = {
        red: 'rgb(255, 99, 132)',
        orange: 'rgb(255, 159, 64)',
        yellow: 'rgb(255, 205, 86)',
        green: 'rgb(11,150,93)',
        blue: 'rgb(54, 162, 235)',
        purple: 'rgb(153, 102, 255)',
        grey: 'rgb(152,154,157)',
        maroon: 'rgb(222,63,37)'
    };


    var config1 = {
        type: 'doughnut',
        data: {
            datasets: [{
                data: <?php echo json_encode($nowNumbers);?>,
                backgroundColor: [
                    window.chartColors.maroon,
                    window.chartColors.green,
                    window.chartColors.purple,
                    window.chartColors.grey
                ],
                label: 'Dataset 1'
            }],
            labels: [
                'GR',
                'SP',
                'AKD',
                'Other'
            ]
        },
        options: {
            responsive: true,
            legend: {
                position: 'bottom',
            },
            title: {
                display: true,
                text: '2019'
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    };
    var config2 = {
        type: 'doughnut',
        data: {
            datasets: [{
                data: <?php echo json_encode($OldNumbers);?>,
                backgroundColor: [
                    window.chartColors.blue,
                    window.chartColors.yellow,
                    window.chartColors.grey
                ],
                label: 'Dataset 1'
            }],
            labels: [
                'MR',
                'MS',
                'Other'
            ]
        },
        options: {
            responsive: true,
            legend: {
                position: 'bottom',
            },
            title: {
                display: true,
                text: '<?php echo $election;?>'
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    };
    var config3 = {
        type: 'doughnut',
        data: {
            datasets: [{
                data: <?php echo json_encode($old18Numbers);?>,
                backgroundColor: [
                    window.chartColors.blue,
                    window.chartColors.yellow,
                    window.chartColors.grey
                ],
                label: 'Dataset 1'
            }],
            labels: [
                'SLPP',
                'UNP',
                'Other'
            ]
        },
        options: {
            responsive: true,
            legend: {
                position: 'bottom',
            },
            title: {
                display: true,
                text: '<?php echo $election;?>'
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    };

    window.onload = function () {
        var ctx = document.getElementById('chart-area').getContext('2d');
        var ctx2 = document.getElementById('chart-area2').getContext('2d');
        var ctx3 = document.getElementById('chart-area3').getContext('2d');
        window.myDoughnut = new Chart(ctx, config1);
        window.myDoughnut2 = new Chart(ctx2, config2);
        window.myDoughnut3 = new Chart(ctx3, config3);
    };


</script>

