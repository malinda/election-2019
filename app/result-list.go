<?php

include_once 'common_ui.go';
$title = $title . " - Dashboard";
include_once 'views/header' . $URL_Extension;


?>

<div id="app">
    <h2>Result by Electorate</h2>

<a href="result.go" class="btn btn-primary btn-sm">Add</a>
    <div class="table-responsive">
        <table class="table table-striped " id="myTable">
            <thead>
            <tr>
                <th>Electorate</th>
                <th>2019 Votes</th>
                <th>2019 %</th>
                <th>2015 Votes</th>
                <th>2015 %</th>
                <th>2018 Votes</th>
                <th>2018 %</th>
                <th>District</th>
                <th>Edit</th>

            </tr>
            </thead>

            <tbody>
            <tr v-for="row in item">
                <td>{{row.NAME}} <span class="badge badge-info" v-if="row.POSTAL==1">Postal</span> </td>
                <td>{{row.V_NEW}}</td>
                <td>{{row.V_NEW_P}}%</td>
                <td>{{row.V_OLD}}</td>
                <td>{{row.V_OLD_P}}%</td>
                <td>-</td>
                <td>-</td>
                <td>{{row.DISTRICT}}</td>
                <td>
                    <a v-bind:href="'result.go?section=' + row.NAME + '&district=' + row.DISTRICT + '&postal=' + row.POSTAL " class="btn btn-success btn-sm adminbtn">Edit</a>
                    <a v-bind:href="'summery-section.go?section=' + row.NAME + '&district=' + row.DISTRICT + '&postal=' + row.POSTAL " class="btn btn-sm btn-success">View</a>
                </td>

                </td>


            </tr>

            </tbody>
        </table>

    </div>

</div>

<?php

include_once 'views/footer' . $URL_Extension;
if($_SESSION['aq_log_user']['TYPE'] != "ADMIN"){
    echo "<style>.adminbtn{display: none}</style>";
}
?>
<script src="static/js/jquery.dataTables.min.js"></script>
<link href="static/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="static/css/datatable.bootstrap.css" rel="stylesheet">

<script>
    var tempate = 1;
    var app = new Vue({
        el: '#app',
        data: {
            item: []
        },
        methods: {
            loadData: function () {
                var self = this;
                $.getJSON(apibase + "api/result-table.go", function (data) {

                    if (data.code === 2000) {
                        // debugger;
                        self.item = data.data
                        buildTable();

                    } else {
                        handleError(data)
                    }

                });
            },

            getAgo: function (time) {
                // debugger;
                console.log(time);

                return moment(time + " +5:30", "YYYY-MM-DD HH:mm:ss Z").fromNow();
            },
            loadItem: function (id) {
                loadItem(id);
            }
        }, mounted: function () {
            //this.loadData();

            this.loadData();

        }
    });

    function buildTable() {
        $(document).ready(function () {
            $('#myTable').DataTable();
        });
    }
</script>

