<?php
include_once 'common_ui.go';
$title = $title . " - Login";
$msg = "";
$style = "primary";

if (isset($_GET['url'])) {
    $_SESSION['aq_logurl'] = $_GET['url'];
}
if (isset($_GET['logout'])) {
    session_destroy();
    session_commit();

    $style = "success";
    $msg = $STR_LOGIN_LOGOUTDONE;
} elseif ((isset($_SESSION['aq_loginstatus']) && $_SESSION['aq_loginstatus'] == 1)) {
    header("Location: " . "index".$URL_Extension);

} elseif (isset($_GET['expire'])) {
    session_destroy();

    if($_GET['expire']==1){
        $style = "danger";
        //$msg = $STR_LOGIN_RELOGIN;
    }

} else if (isset($_POST['username']) && isset($_POST['password'])) {
    $username = $_POST['username'];
    $passowrd = $_POST['password'];
    makelog("LoginAttempt : ".$username ." " .$_SERVER['REMOTE_ADDR']  . " " .  $_SERVER['HTTP_USER_AGENT'],"LOG");
    $sql = "SELECT * FROM `login` WHERE (`username` = ? OR `email` = ?) AND password = SHA1(?)";
    if ($stmt = $conn->prepare($sql)) {
        $stmt->bind_param("sss", $username, $username, $passowrd);

        $result = $stmt->execute();

        $result = $stmt->get_result();
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $user = $row;
                if ($user['STATUS'] == "ACTIVE") {
                    makelog("Active User : ".$username ,"LOG");
                    $url = "";
                    if (isset($_SESSION['aq_logurl'])) {
                        $url = $_SESSION['aq_logurl'];
                    }
                    session_destroy();
                    //session_commit();
                    session_start();
                    // session_commit();

                    if ($url != '') {
                        $_SESSION['aq_logurl'] = $url;
                    }
                    $_SESSION['aq_loginstatus'] = 1;
                    $_SESSION['aq_log_user'] = $user;

                    if ($url != '') {
                        $_SESSION['aq_logurl'] = $url;
                        header("Location: " . $url);

                    }else{
                        header("Location: " . "index".$URL_Extension);

                    }
                } else {
                    $style = "danger";
                    $msg = $STR_LOGIN_NOTACTIVE;
                    makelog("Inactive User : ".$username ,"LOG");
                }

            }
        } else {
            $style = "danger";
            $msg = $STR_LOGIN_WRONGUSERNAME;

        }
    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $title; ?> </title>

    <!-- Bootstrap core CSS -->
    <link href="static/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="static/css/signin.css" rel="stylesheet">
</head>

<body class="text-center">
<form class="form-signin" method="post" action="login.go">
    <img class="mb-4" src="static/img/YPAC-C.png" alt="" width="90" >
    <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
    <?php
    if ($msg != "") {
        ?>

        <div class="card text-white bg-<?php echo $style ?> text-center">
            <div class="card-body">
                <?php echo $msg; ?>
            </div>
        </div>
        <br>

        <?php
    }

    ?>
    <label for="inputEmail" class="sr-only">Username</label>
    <input type="text" id="inputEmail" class="form-control" placeholder="Username" name="username" required autofocus>
    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" id="inputPassword" class="form-control" placeholder="Password" name="password" required>
<!--    <div class="checkbox mb-3">-->
<!--        <label>-->
<!--            <input type="checkbox" value="remember-me"> Remember me-->
<!--        </label>-->
<!--    </div>-->
    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    <p class="mt-5 mb-3 text-muted">&copy; 2019 Young Professionals Political Action Committee (YPAC)</p>
</form>
</body>
</html>
