<?php

include_once 'common_ui.go';
$title = $title . " - Dashboard";
include_once 'views/header' . $URL_Extension;


?>

<div id="app">
    <h2>All List</h2>


        <div class="table-responsive">
            <table class="table table-striped " id="myTable">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Code</th>
                    <th>Number</th>
                    <th>Division</th>
                    <th>District</th>
                    <th>Province</th>
                    <th>Population</th>
                    <th>Sinhala</th>
                    <th>Muslim</th>
                    <th>Tamil</th>
                    <th>Other</th>
                    <th>House</th>



                </tr>
                </thead>
                <tbody>


                </tbody>
            </table>

        </div>

</div>

<?php

include_once 'views/footer' . $URL_Extension;
include 'info.modal.go';

?>
<script src="static/js/jquery.dataTables.min.js"></script>
<link href="static/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="static/css/datatable.bootstrap.css" rel="stylesheet">

<script>
//     var tempate = 1;
//     var app = new Vue({
//         el: '#app',
//         data: {
//             item: []
//         },
//         methods: {
//             loadData: function () {
//                 var self = this;
//                 $.getJSON(apibase + "api/get-list.go", function (data) {
//
//                     if (data.code === 2000) {
//                         // debugger;
//                         self.item = data.data
// buildTable();
//
//                     } else {
//                         handleError(data)
//                     }
//
//                 });
//             },
//
//             getAgo: function (time) {
//                 // debugger;
//                 console.log(time);
//
//                 return moment(time + " +5:30", "YYYY-MM-DD HH:mm:ss Z").fromNow();
//             },
//             loadItem: function(id){
//                 loadItem(id);
//             }
//         }, mounted: function () {
//             //this.loadData();
//
//             this.loadData();
//
//         }
//     });
var table = null;

    function buildTable(){
        $(document).ready( function () {
             table =  $('#myTable').DataTable({
                serverSide: true,
                 processing: true,
                ajax: 'api/data-table.go'
            });

            $('#myTable tbody').on( 'click', 'tr', function () {
                var indx = table.row( this ).index();
                var row = table.row(indx).data();

                loadItem(row.rowid);
                $('#infoModal').modal('show');
            } );
        } );
    }
    buildTable();
</script>

