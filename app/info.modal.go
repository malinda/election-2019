<!-- Logout Modal-->
<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="#modalHead">?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" id="modalBody"></div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>

            </div>
        </div>
    </div>
</div>
<script>
    function loadItem(id){
        $.get("api/get-data.go?GNDID=" + id, function (data, status) {
            setData(data);
        })
    }
    $('#infoModal').on('show.bs.modal', function () {

    })

    function setData(data){
        $(".modal-title").html(data.data.gnd_name );

        var d = "NO : " + data.data.gnd_number + "<br><br>"+
            "Population : " + data.data.D1 + "<br>"+
            "Sinhala : " + data.data.D2 + "<br>"+
            "Muslim : " + data.data.D3 + "<br>" +
            "Tamil : " + data.data.D3 + "<br>" +
            "Other : " + data.data.D3 + "<br>" +
            "House : " + data.data.D3 + "<br><br>"+
            "gnd_name : " + data.data.gnd_name + "<br>"+
            "gnd_code : " + data.data.gnd_code + "<br>"+
            "gnd_number : " + data.data.gnd_number + "<br>"+
            "ds_division_name : " + data.data.ds_division_name + "<br>"+
            "ds_division_code : " + data.data.ds_division_code + "<br>"+
            "district_name : " + data.data.district_name + "<br>"+
            "district_code : " + data.data.district_code + "<br>"+
            "province_name : " + data.data.province_name + "<br>"+
            "province_code : " + data.data.province_code + "<br>"+
            "mc_uc_pc_name : " + data.data.mc_uc_pc_name + "<br>"+
            "gnd_officer_name : " + data.data.gnd_officer_name + "<br>"+
            "gnd_officer_phone : " + data.data.gnd_officer_phone + "<br>"+
            "province_census_code : " + data.data.province_census_code + "<br>"+
            "district_census_code : " + data.data.district_census_code + "<br>"+
            "ds_division_census_code : " + data.data.ds_division_census_code + "<br>"+
            "gnd_census_code : " + data.data.gnd_census_code + "<br>"+
            "gnd_no_census : " + data.data.gnd_no_census + "<br>"+
            "gnd_name_census : " + data.data.gnd_name_census + "<br>"+
            "gnd_no_gazetted : " + data.data.gnd_no_gazetted + "<br>"
        $("#modalBody").html(d);
    }
</script>