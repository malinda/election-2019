<?php

include_once 'common_ui.go';
$title = $title . " - Dashboard";
include_once 'views/header' . $URL_Extension;


?>

<div id="app">
    <h2>Electorate Result - Presidential Election 2019</h2>

<a href="result.go" class="btn btn-primary btn-sm">Add</a>
    <div class="table-responsive">
        <table class="table table-striped " id="myTable">
            <thead>
            <tr>
                <th>Electorate</th>
                <th>GR Votes</th>
                <th>GR Postal Votes</th>
                <th>SP Votes</th>
                <th>SP Postal Votes</th>
                <th>District</th>
                <th>More</th>

            </tr>
            </thead>

            <tbody>
            <tr v-for="row in item">
                <td>{{row.NAME}} <span class="badge badge-info" v-if="row.POSTAL==1">Postal</span> </td>
                <td>{{row.P_2019_V}}</td>
                <td>{{row.P_2019_VP}}</td>
                <td>{{row.P_2019SP_V}}</td>
                <td>{{row.P_2019SP_VP}}</td>
                <td>{{row.DISTRICT}}</td>
                <td>

                    <a v-bind:href="'summery-section.go?section=' + row.NAME + '&district=' + row.DISTRICT + '&postal=' + row.POSTAL " class="btn btn-success">View</a>
                </td>

                </td>


            </tr>

            </tbody>
        </table>

    </div>

</div>

<?php

include_once 'views/footer' . $URL_Extension;
include 'info.modal.go';

?>
<script src="static/js/jquery.dataTables.min.js"></script>
<link href="static/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="static/css/datatable.bootstrap.css" rel="stylesheet">

<script>
    var tempate = 1;
    var app = new Vue({
        el: '#app',
        data: {
            item: []
        },
        methods: {
            loadData: function () {
                var self = this;
                $.getJSON(apibase + "api/last-update.go", function (data) {

                    if (data.code === 2000) {
                        // debugger;
                        self.item = data.data
                        buildTable();

                    } else {
                        handleError(data)
                    }

                });
            },

            getAgo: function (time) {
                // debugger;
                console.log(time);

                return moment(time + " +5:30", "YYYY-MM-DD HH:mm:ss Z").fromNow();
            },
            loadItem: function (id) {
                loadItem(id);
            }
        }, mounted: function () {
            //this.loadData();

            this.loadData();

        }
    });

    function buildTable() {
        $(document).ready(function () {
            $('#myTable').DataTable();
        });
    }
</script>

