<?php
/**
 * Created by IntelliJ IDEA.
 * User: malin
 * Date: 2019-07-09
 * Time: 11:09 AM
 */

$title = "GN ";
$long_tittle = "GN";



$LOG_PATH = "log/";
$URL_Extension  =".go";

$CACHE_Expire = 3600;
$CACHE_ExpireLatest = 300;
$CACHE_PATH ="";
$CACHE =true;
$UI_LOAD_INTERVAL = 60000;

$supportGR = ["SLPP","SLFP","UPFA","EPDP","Mahinda Rajapaksa"];
$supportSP = ["UNP","SLMC","ACMC","ITAK","Maithripala Sirisena"];
$supportAKD = ["JVP"];


$name_GR = "Gotabaya Rajapaksa";
$name_SP = "Sajith Premadasa";
$name_AKD = "Anura Kumara Disanayake";


$candidates = ["Gotabaya Rajapaksa","Sajith Premadasa","Anura Kumara Disanayake"];