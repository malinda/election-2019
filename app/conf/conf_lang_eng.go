<?php
/**
 * Created by IntelliJ IDEA.
 * User: malin
 * Date: 2019-07-09
 * Time: 12:02 PM
 */
$STR_LOGIN_WRONGUSERNAME = "Wrong password";
$STR_LOGIN_NOTACTIVE = "Account not activated";
$STR_LOGIN_LOGOUTDONE = "Logout Success";
$STR_LOGIN_RELOGIN = "Session Expired, Please login again";
$STR_PWRESET_NOTMATCH = "Password not matched";
$STR_PWRESET_OLDPASSWORD = "New password same as old one";
$STR_PWRESET_MINLEN = "Password not meet min length";
$STR_PWRESET_UPDATED = "Password updated";
$STR_PWRESET_NOTUPDATED = "Password not updated";