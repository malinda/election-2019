<?php
/**
 * Created by IntelliJ IDEA.
 * User: malin
 * Date: 2019-08-21
 * Time: 12:06 PM
 */
include_once 'common_API.php';
// DB table to use
$table = 'datatable';

// Table's primary key
$primaryKey = 'ID';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
    array( 'db' => 'ID', 'dt' => "rowid" ),
    array( 'db' => 'gnd_name', 'dt' => 0 ),
    array( 'db' => 'gnd_code',  'dt' => 1 ),
    array( 'db' => 'gnd_number',  'dt' => 2 ),
    array( 'db' => 'ds_division_name',  'dt' => 3 ),
    array( 'db' => 'district_name',  'dt' => 4 ),
    array( 'db' => 'province_name',  'dt' => 5 ),
    array( 'db' => 'D1',  'dt' => 6 ),
    array( 'db' => 'D2',  'dt' => 7 ),
    array( 'db' => 'D3',  'dt' => 8 ),
    array( 'db' => 'D4',  'dt' => 9 ),
    array( 'db' => 'D5',  'dt' => 10 ),
    array( 'db' => 'D6',  'dt' => 11 )
);
//ID,gnd_name,gnd_code,gnd_number,ds_division_name,district_name,province_name,`data`.*

$sql_details = array(
    'user' => $username,
    'pass' => $password,
    'db'   => $dbname,
    'host' => $servername
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( 'datatable/ssp.class.php' );

echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);