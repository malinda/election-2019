<?php
/**
 * Created by IntelliJ IDEA.
 * User: malin
 * Date: 2019-07-10
 * Time: 11:28 AM
 */
$URL_Extension  =".go";
include_once '../common.go';
include_once 'errors.php';
include_once 'functions.php';



include_once 'curl.php';


if (isset($_SESSION['aq_loginstatus']) && $_SESSION['aq_loginstatus'] == 1) {
    $user_name = $_SESSION['aq_log_user']['NAME'];
    $user_id = $_SESSION['aq_log_user']['ID'];
} else {

    $response['status'] = "ERROR";
    $response['msg'] = $STATUS_ERROR_AUTHERROR_MSG;
    $response['code'] = $STATUS_ERROR_AUTHERROR;
    $response["data"] = null;

    echo json_encode($response);
    exit;
}