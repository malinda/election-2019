<?php
/**
 * Created by IntelliJ IDEA.
 * User: malin
 * Date: 2019-08-21
 * Time: 12:06 PM
 */
include_once 'common_API.php';

$id = 0;

$election = "2015";

$sql = "SELECT NAME, TOTAL_POLLED,VALID_V,VALID_V_P,TOTAL_POLLED_P,VOTE,PERCENTAGE,`result_section`.DISTRICT,POSTAL FROM `result_section` ,`result_candidate` WHERE `result_candidate`.SECTION_ID  = `result_section`.ID AND CANDIDATE = 'Mahinda Rajapaksa' AND ELECTION=?" ;
$out = [];

if ($stmt = $conn->prepare($sql)) {
    $stmt->bind_param("s",$election);


    $result = $stmt->execute();

    $result = $stmt->get_result();


    while ($row = $result->fetch_assoc()) {
        //$row['TEMPLATEQTY'] = 0;
        $row['V_OLD'] = $row['VOTE'];
        $row['V_OLD_P'] = $row['PERCENTAGE'];
        $row['V_NEW'] = "0";
        $row['V_NEW_P'] = "0";
        $out[] = $row;



    }
}

showResponse($STATUS_OK_MSG, $STATUS_OK, $out, "");