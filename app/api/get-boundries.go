<?php
/**
 * Created by IntelliJ IDEA.
 * User: malin
 * Date: 2019-08-21
 * Time: 12:06 PM
 */
include_once 'common_API.php';

$id = 0;

if(isset($_GET['GNDID'])){
    $id = $_GET['GNDID'];
}else{
    exit();
}

$sql = "SELECT *  FROM `geo` WHERE `GNDID` = ? ";
$out = [];

if ($stmt = $conn->prepare($sql)) {
     $stmt->bind_param("s", $id);

    $result = $stmt->execute();

    $result = $stmt->get_result();


    while ($row = $result->fetch_assoc()) {
        //$row['TEMPLATEQTY'] = 0;
        $out[] = $row;


    }
}

showResponse($STATUS_OK_MSG, $STATUS_OK, $out, "");