<?php
/**
 * Created by IntelliJ IDEA.
 * User: malin
 * Date: 2019-07-10
 * Time: 11:47 AM
 */
function showError($msg, $code)
{
    $response['status'] = "ERROR";
    $response['msg'] = $msg;
    $response['code'] = $code;
    $response["data"] = null;

    echo json_encode($response);
    exit;
}

function showResponse($msg, $code, $data, $method)
{
    header("Content-type: application/json");
    $response['status'] = "SUCCESS";
    $response['msg'] = $msg;
    $response['code'] = $code;
    $response["data"] = $data;
    $response['time'] = time();

    echo json_encode($response);


}

function showCacheResponse($body)
{
    header("Content-type: application/json");


    echo json_encode($body);

}

function loadCache($method, $cacheTime)
{
    global $CACHE_PATH, $CACHE;
    if (!$CACHE)
        return $CACHE;
    if (file_exists($CACHE_PATH . "cache/" . $method . ".json")) {
        $f = file_get_contents("cache/" . $method . ".json");
        $json = json_decode($f);
        if ($json->time > (time() - $cacheTime)) {
            showCacheResponse($json);
            return true;
        }
    }

    return false;
}

function makelog($msg, $type)
{
    global $LOG_PATH;
//    echo $LOG_PATH.date("Y-m-d").".log";
//    echo getcwd();
    file_put_contents($LOG_PATH . date("Y-m-d") . ".log", date("Y-m-d H:i:s") . " : ".session_id()." " . $msg . PHP_EOL, FILE_APPEND);
}

function getProfileCode()
{
    global $RAIN_PROFILE;
    return $RAIN_PROFILE[0];
}

//function getTokenByDeviceCode($code){
//    if ((!isset($data['status'])) || $data['status'] != "OK" || $data['statusCode'] != 200) {
//        showError($STATUS_ERROR_APIFAILED, $STATUS_ERROR_APIFAILED_MSG);
//    }
//    $device = json_decode($data['body'])->Devices;
//
//    $result = array();
//    for ($x = 0; $x < count($device); $x++) {
//}

function getIcon($profile){
    global $RAIN_PROFILE_ICON;

    if(isset($RAIN_PROFILE_ICON[$profile]))
        return $RAIN_PROFILE_ICON[$profile]['iconprefix'];

    return "";
}

