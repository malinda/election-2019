<?php

include_once 'common_ui.go';
$title = $title . " - Dashboard";

$dis = "";
$sel = "";
$postal = "0";

if($_SESSION['aq_log_user']['TYPE'] != "ADMIN"){
    echo " Please login using editable user";
    exit();
}
if (isset($_GET['section']))
    $sel = $_GET['section'];
if (isset($_GET['district']))
    $dis = $_GET['district'];
if (isset($_GET['postal']))
    $postal = $_GET['postal'];

//$candidates = getCandidates("2015");
$sections = getSections("2015");
$districts = getDistricts();

if (isset($_POST['POSTAL'])) {
    $postal = $_POST['POSTAL'];
    $district = $_POST['DISTRICT'];
    $gr = $_POST['GR'];
    $sp = $_POST['SP'];
    $akd = $_POST['AKD'];
    $section = $_POST['SECTION'];
    $c1n = $_POST['C1_N'];
    $c1v = $_POST['C1_V'];
    $other = $_POST['other'];
    $total = $_POST['total'];
    $room = $_POST['room'];
    if($room=="")
        $room = "0";
//    $c2n = $_POST['C2_N'];
//    $c2v = $_POST['C2_V'];

    if($gr > 0 && $section != "" ) {
        $other = $total - ($gr+$sp+$akd) ;
        if($c1n !="" && $c1v>0){
            $other -=  $c1v;
        }

        $sectionID = updateSection($section, $postal, $gr,$sp,$district,$total);

        addResult($section, $name_GR, $gr, $postal, $sectionID,$total,$room);
        addResult($section, $name_SP, $sp, $postal, $sectionID,$total,$room);
        addResult($section, $name_AKD, $akd, $postal, $sectionID,$total,$room);
        addResult($section, "Other", $other, $postal, $sectionID,$total,$room);

        if($c1n !="" && $c1v>0){
            addResult($section, $c1n, $c1v, $postal, $sectionID,$total,$room);

        }

        header("Location: summery-section.go?section=".$section);
    }

}
include_once 'views/header' . $URL_Extension;

?>

<div id="app">
    <div class="row">
        <div class="card card-body col-sm-8 ">
            <form method="post">

                <div class="form-group">
                    <label for="Option1">District</label>
                    <select class="form-control" name="DISTRICT">
                        <?php


                        for ($x = 0; $x < count($districts); $x++) {
                            if ($dis != "" && $dis == $districts[$x]['DISTRICT']) {
                                echo '<option value="' . $districts[$x]['DISTRICT'] . '" selected>' . $districts[$x]['DISTRICT'] .'</option>';
                            } else {
                                echo '<option value="' . $districts[$x]['DISTRICT'] . '" >' . $districts[$x]['DISTRICT'] .'</option>';
                            }
                        }
                        ?>

                    </select>

                </div>
                <div class="form-group">
                    <label for="Option1">Section</label>
                    <select class="form-control" name="SECTION">
                        <?php

                        for ($x = 0; $x < count($sections); $x++) {
                            if ($sel != "" && $sel == $sections[$x]['NAME']) {
                                echo '<option value="' . $sections[$x]['NAME'] . '" selected>' . $sections[$x]['NAME'] . ' [' . $sections[$x]['P_2019_V'] . ']</option>';
                            } else {
                                echo '<option value="' . $sections[$x]['NAME'] . '">' . $sections[$x]['NAME'] . ' [' . $sections[$x]['P_2019_V'] . ']</option>';
                            }
                        }
                        ?>

                    </select>

                </div>
                <div class="form-group">
                    <label for="Option1">Postal Vote</label>
                    <select class="form-control" name="POSTAL">
                        <option value="0">No</option>
                        <option value="1" <?php if($postal==1) echo "selected";?>>Yes</option>

                    </select>

                </div>
                <div class="form-group">
                    <label for="GR">GotabayaR</label>
                    <input type="number" class="form-control" id="gr" aria-describedby="gr" placeholder="vote"
                           name="GR">
                    <!--               <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                </div>
                <div class="form-group">
                    <label for="Sajith">SajithP</label>
                    <input type="number" class="form-control" id="gr" aria-describedby="gr" placeholder="vote"
                           name="SP">
                    <!--               <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                </div>
                <div class="form-group">
                    <label for="GR">AKD</label>
                    <input type="number" class="form-control" id="gr" aria-describedby="gr" placeholder="vote"
                           name="AKD" >
                    <!--               <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                </div>
                <div class="form-group row">
                    Total</label>

                    <input type="number" class="form-control col-sm-3" id="gr" aria-describedby="gr" placeholder="total"
                           name="total" required>
                    <!--               <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                </div>
                <br>
                <div class="form-group row">
                        Room</label>

                    <input type="number" class="form-control col-sm-3" id="gr" aria-describedby="gr" placeholder="room"
                           name="room" >
                    <!--               <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                </div>
                <br>

                <div class="form-group row">
                    <label for="Option1" class="col-sm-2">Candidate</label>
                    <select class="form-control col-sm-7" name="C1_N">
                        <option value=""></option>

                        <?php
                        for ($x = 0; $x < count($candidates); $x++) {
                            echo '<option value="' . $candidates[$x]. '">' . $candidates[$x] . '</option>';
                        }
                        ?>
                    </select>
                    <input type="number" class="form-control col-sm-3" id="gr" aria-describedby="gr" placeholder="vote"
                           name="C1_V">
                    <!--               <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>

        </div>
        <div class="card card-body col-sm-4 ">
<!--            <h3> Walapane</h3>-->
<!---->
<!--            2015 Result-->
<!--            <table class="table">-->
<!--                <tr>-->
<!--                    <td>Maithripala Sirisena</td>-->
<!--                    <td>0</td>-->
<!--                    <td>0%</td>-->
<!--                </tr>-->
<!--                <tr>-->
<!--                    <td>Mahinda Rajapaksa</td>-->
<!--                    <td>0</td>-->
<!--                    <td>0%</td>-->
<!--                </tr>-->
<!--            </table>-->

        </div>
    </div>

</div>

<?php

include_once 'views/footer' . $URL_Extension;
include 'info.modal.go';

?>
<script src="static/js/jquery.dataTables.min.js"></script>
<link href="static/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="static/css/datatable.bootstrap.css" rel="stylesheet">



