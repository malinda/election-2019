<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.go" style="background-color: white;">
        <div class="sidebar-brand-icon ">
<!--            <i class="fas fa-laugh-wink"></i>-->

            <img src="static/img/YPAC-C.png" width="50px">
        </div>
<!--        <div class="sidebar-brand-text mx-3">GN</div>-->
    </a>


    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="index.go">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">


    <!-- Nav Item - Charts -->
    <li class="nav-item">
        <a class="nav-link" href="last-update.go">
            <i class="fas fa-fw fa-table"></i>
            <span>Electorate Result</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="result-list.go">
            <i class="fas fa-fw fa-table"></i>
            <span>Result by Electorate</span></a>
    </li>

    <!-- Nav Item - Tables -->
    <li class="nav-item">
        <a class="nav-link" href="summery1.go">
            <i class="fas fa-fw fa-calendar"></i>
            <span>Summary</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>