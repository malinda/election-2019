import json
import mysql.connector

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="dialog@123",
    database="gn",
    charset='utf8',
    use_unicode=True
)
mycursor = mydb.cursor()

with open('SLNSDI2.json') as json_file:
    data = json.load(json_file)
    x = 0

    for pagec in range(len(data['dataPages'])):
        print("Starting ",pagec)
        for key in data['dataPages'][pagec]['features']:
            x += 1
            print(x)
            cur = key['attributes']
            cur['st_area'] = cur['st_area(shape)']
            del cur['st_area(shape)']
            cur['st_length'] = cur['st_length(shape)']
            del cur['st_length(shape)']
            cur['gnd_name_gazetted'] = json.dumps(cur['gnd_name_gazetted'])
            cur['gnd_name_gazetted'] = cur['gnd_name_gazetted'].replace('"',"")
            cur['gnd_name_gazetted'] = cur['gnd_name_gazetted'].replace("u","\\u")
            cur['gnd_name'] = cur['gnd_name'].replace('"',"'")
            columns = ', '.join(cur.keys())
            placeholders = ', '.join('"{0}"'.format(w) for w in cur.values())
            sql = 'INSERT INTO gnd (`ID`,{}) VALUES (null, {})'.format(columns, placeholders)
            mycursor.execute(sql)
            mydb.commit()
            id = mycursor.lastrowid
            s = "INSERT INTO `geo` (`ID`, `GNDID`, `lat`, `lon`) VALUES (NULL, {}, {}, {});"

            for geo in key['geometry']['rings'][0]:
                q = s.format(id, geo[1], geo[0])
                mycursor.execute(q)
            mydb.commit()
            # print(mycursor.rowcount, "record inserted.")
