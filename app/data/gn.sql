-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 27, 2019 at 10:19 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gn`
--

-- --------------------------------------------------------

--
-- Table structure for table `geo`
--

CREATE TABLE IF NOT EXISTS `geo` (
  `ID` int(20) NOT NULL AUTO_INCREMENT,
  `GRNDI` int(20) NOT NULL,
  `lat` double NOT NULL,
  `lon` double NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gnd`
--

CREATE TABLE IF NOT EXISTS `gnd` (
  `ID` int(20) NOT NULL AUTO_INCREMENT,
  `objectid` int(50) NOT NULL,
  `gnd_name` varchar(200) NOT NULL,
  `gnd_code` varchar(100) NOT NULL,
  `gnd_number` varchar(50) NOT NULL,
  `gnd_name_gazetted` varchar(500) NOT NULL,
  `ds_division_name` varchar(200) NOT NULL,
  `ds_division_code` varchar(20) NOT NULL,
  `district_name` varchar(100) NOT NULL,
  `district_code` varchar(20) NOT NULL,
  `province_name` varchar(100) NOT NULL,
  `province_code` varchar(20) NOT NULL,
  `mc_uc_pc_name` varchar(100) NOT NULL,
  `gnd_officer_name` varchar(200) NOT NULL,
  `gnd_officer_phone` varchar(50) NOT NULL,
  `dispute` varchar(200) NOT NULL,
  `province_census_code` varchar(20) NOT NULL,
  `district_census_code` varchar(20) NOT NULL,
  `ds_division_census_code` varchar(20) NOT NULL,
  `gnd_census_code` varchar(20) NOT NULL,
  `gnd_no_census` varchar(50) NOT NULL,
  `gnd_name_census` varchar(50) NOT NULL,
  `admin_code` varchar(50) NOT NULL,
  `cm_index` varchar(50) NOT NULL,
  `gnd_no_gazetted` varchar(50) NOT NULL,
  `year_created` varchar(50) NOT NULL,
  `data_source` varchar(200) NOT NULL,
  `method_of_creation` varchar(200) NOT NULL,
  `extent` double NOT NULL,
  `st_area` double NOT NULL,
  `st_length` double NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `USERNAME` varchar(30) NOT NULL,
  `EMAIL` varchar(50) NOT NULL,
  `NAME` varchar(200) NOT NULL,
  `PASSWORD` varchar(100) NOT NULL,
  `STATUS` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `email` (`EMAIL`),
  UNIQUE KEY `username_2` (`USERNAME`),
  KEY `username` (`USERNAME`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`ID`, `USERNAME`, `EMAIL`, `NAME`, `PASSWORD`, `STATUS`) VALUES
(1, 'malinda', 'malinda555@gmail.com', 'Malinda', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'ACTIVE'),
(2, 'dulantha', 'dulantha@xdoto.io', 'Dulantha', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'ACTIVE'),
(3, 'thusitha', '-', 'Thusitha', 'c9f4dcf9bed2afc6179ae8241ace16b8b9580318', 'ACTIVE'),
(4, 'sampath', 'sam', 'Sampath', 'd1b13ee13de25331f067da93426ee2ecd1584792', 'ACTIVE');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
