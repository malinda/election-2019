<?php
/**
 * Created by IntelliJ IDEA.
 * User: malin
 * Date: 2019-07-10
 * Time: 10:38 AM
 */

$URL_Extension  =".go";

include_once 'common'.$URL_Extension;
include_once 'functions'.$URL_Extension;
//include_once 'conf/conf_rainfall.php';
//
$user_name = "";
$user_id = "";
//var_dump($_SESSION);
if (isset($_SESSION['aq_loginstatus']) && $_SESSION['aq_loginstatus'] == 1) {
    $user_name = $_SESSION['aq_log_user']['NAME'];
    $user_id = $_SESSION['aq_log_user']['ID'];
} else {
    if (str_replace("login".$URL_Extension, "", $_SERVER['REQUEST_URI']) != $_SERVER['REQUEST_URI']) {
        //login page
    } else {
        header("Location: " . "login".$URL_Extension."?expire=1&url=" . urlencode($actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}"));
        exit;
    }

}