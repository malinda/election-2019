<?php
/**
 * Created by IntelliJ IDEA.
 * User: malin
 * Date: 2019-08-05
 * Time: 6:30 PM
 */

function getProfileCode()
{
    global $RAIN_PROFILE;
    return $RAIN_PROFILE[0];
    //return implode(",",$RAIN_PROFILE);
}

function makelog($msg, $type)
{
    global $LOG_PATH;
//    echo $LOG_PATH.date("Y-m-d").".log";
//    echo getcwd();
    file_put_contents($LOG_PATH . date("Y-m-d") . ".log", date("Y-m-d H:i:s") . " : " . session_id() . " " . $msg . PHP_EOL, FILE_APPEND);
}

function generateBatch($reactor, $date)
{
    //select

    return date("Ymd", strtotime($date)) . "R" . $reactor . "B1";
}

function getGroups()
{
    global $conn;
    $sql = "SELECT * FROM `group` ORDER BY NAME DESC ";


    $out = [];

    if ($stmt = $conn->prepare($sql)) {


        $result = $stmt->execute();


        $result = $stmt->get_result();


        while ($row = $result->fetch_assoc()) {

            $out[] = $row;

        }
    }

    return $out;
}

function getBrand()
{
    global $conn;
    $sql = "SELECT * FROM `brand` ORDER BY NAME DESC ";


    $out = [];

    if ($stmt = $conn->prepare($sql)) {


        $result = $stmt->execute();


        $result = $stmt->get_result();


        while ($row = $result->fetch_assoc()) {

            $out[] = $row;

        }
    }

    return $out;
}


function getTypes()
{
    global $conn;
    $sql = "SELECT * FROM `type` ORDER BY NAME DESC ";


    $out = [];

    if ($stmt = $conn->prepare($sql)) {


        $result = $stmt->execute();


        $result = $stmt->get_result();


        while ($row = $result->fetch_assoc()) {

            $out[] = $row;

        }
    }

    return $out;
}

function getDailyJobCont($date, $reactor)
{
    global $conn;
    $sql = "SELECT count(*) as c FROM `production` WHERE `date` >= ? AND `date`<= ? and REACTOR =?";

    if ($stmt = $conn->prepare($sql)) {
        $start = $date . " 00:00:00";
        $stop = $date . " 23:59:59";
        $stmt->bind_param("sss", $start, $stop, $reactor);

        $result = $stmt->execute();
        $result = $stmt->get_result();
        $c = 0;
        while ($row = $result->fetch_assoc()) {

            $c = $row['c'];

        }

        return $c;
    }
    return 0;
}

function addJob($itemId, $qty, $sheet)
{
    global $conn;
    $item = getItem($itemId);

    $packs = 0;
    $loos = $qty;
    if ($item['SETQTY'] != 0) {
        $packs = floor($qty / $item['SETQTY']);
        $loos = $qty - ($packs * $item['SETQTY']);
    }
    $sql = "INSERT INTO `production_item` (`ID`, `PRODUCTION_ID`, `ITEM_ID`, `QTY`, `CHARGESHEET`, `PACKS`,`LOOSE`, `ADJ`) VALUES (NULL, ?, ?, ?, ?, ?,?,0);";
    if ($stmt = $conn->prepare($sql)) {

        $stmt->bind_param("ssssss", $sheet, $itemId, $qty, $item['TYPE'], $packs, $loos);

        $result = $stmt->execute();
    }
}

function getItem($itemId)
{
    global $conn;
    $item = null;
    $sql = "SELECT * FROM `item` WHERE `ID` = ?";
    if ($stmt = $conn->prepare($sql)) {
        $stmt->bind_param("s", $itemId);

        $result = $stmt->execute();
        $result = $stmt->get_result();


        while ($row = $result->fetch_assoc()) {
            $item = $row;

        }
        return $item;


    }
    return null;
}

function getSheet($sheetId)
{
    global $conn;
    $sql = "SELECT * FROM `production` WHERE `BATCHNO` =?; ";
    $sheet = null;
    $items = [];
    if ($stmt = $conn->prepare($sql)) {
        $stmt->bind_param("s", $sheetId);

        $result = $stmt->execute();
        $result = $stmt->get_result();


        while ($row = $result->fetch_assoc()) {
            $sheet = $row;

        }


    }

    if ($sheet == null)
        return null;
    $sql = "SELECT ITEM_ID, NAME, CODE, `production_item`.QTY, `production_item`.CHARGESHEET, `production_item`.PACKS,  `production_item`.LOOSE, `production_item`.ADJ FROM `production_item`,`item` WHERE `item`.ID=ITEM_ID AND PRODUCTION_ID=?;";

    if ($stmt = $conn->prepare($sql)) {
        $stmt->bind_param("s", $sheet['ID']);

        $result = $stmt->execute();
        $result = $stmt->get_result();


        while ($row = $result->fetch_assoc()) {
            $items[] = $row;

        }


    }

    $sheet['ITEMS'] = $items;

    return $sheet;
}


function getChargeSheetName($id)
{
    if ($id == 1)
        return "T1";

    else
        return "T2";
}

function getCurrentSheet($reactor)
{
    global $conn;
    $data1 = date("Y-m-d") . " 00:00:00";
    $data2 = date("Y-m-d") . " 23:59:59";
    $sql = "SELECT * FROM `production` WHERE `DATE` >=?  AND  `DATE` <=?  AND `REACTOR` = ? AND `STATUS` != 'FINISHED'  ORDER BY ID ASC;";

    $sheets = [];
    if ($stmt = $conn->prepare($sql)) {
        $stmt->bind_param("sss", $data1, $data2, $reactor);

        $result = $stmt->execute();
        $result = $stmt->get_result();


        while ($row = $result->fetch_assoc()) {
            $sheets[] = $row;

        }
    }
    if (count($sheets) >= 1)
        return $sheets[0]['BATCHNO'];

    return null;
}

function getQueSheet($reactor)
{
    global $conn;

    $data1 = date("Y-m-d") . " 00:00:00";
    $data2 = date("Y-m-d") . " 23:59:59";
    $sql = "SELECT * FROM `production` WHERE `DATE` >=?  AND  `DATE` <=?  AND `REACTOR` = ? AND `STATUS` != 'FINISHED'  ORDER BY ID ASC;";

    $sheets = [];
    if ($stmt = $conn->prepare($sql)) {
        $stmt->bind_param("sss", $data1, $data2, $reactor);

        $result = $stmt->execute();
        $result = $stmt->get_result();


        while ($row = $result->fetch_assoc()) {
            $sheets[] = $row;

        }
    }
    if (count($sheets) >= 2)
        return $sheets[1]['BATCHNO'];

    return null;
}

function getPendingSheets($reactor)
{
    return [];
}

function updateSheetStatus($sheetId, $status)
{
    global $conn;

    $sql = "UPDATE `production` SET `STATUS` = ? WHERE `production`.`ID` = ?";
    if ($stmt = $conn->prepare($sql)) {
        $stmt->bind_param("ss", $status, $sheetId);

        $result = $stmt->execute();
    }
}

function updateYieldLost($sheetId, $lost, $yield)
{
    global $conn;

    $sql = "UPDATE `production` SET `LOST` = ?, `YEILD` = ? WHERE `production`.`ID` = ?;";
    if ($stmt = $conn->prepare($sql)) {
        $stmt->bind_param("sss", $lost, $yield, $sheetId);

        $result = $stmt->execute();
    }
}

function updateAdj($sheetId, $itemId, $adj)
{
    global $conn;
    $sql = "UPDATE `production_item` SET `ADJ` =`ADJ` + ? WHERE `production_item`.`ITEM_ID` = ? AND PRODUCTION_ID =?";

    if ($stmt = $conn->prepare($sql)) {
        $stmt->bind_param("sss", $adj, $itemId, $sheetId);

        $result = $stmt->execute();
    }

}

function getStatusLevel($statusName)
{
    global $STATUS;

    if (isset($STATUS[$statusName]))
        return $STATUS[$statusName];
    return 0;

}

function getStatusPercentage($statusName)
{
    global $STATUS;

    $s = 0;
    if (isset($STATUS[$statusName]))
        $s = $STATUS[$statusName] + 1;

    if ($s == 0)
        return $s;

    $s = ($s / count($STATUS)) * 100;
    $s = round($s, 0);
    return $s;

}

function getCandidates($election)
{
    global $conn;
    $sql = "SELECT SUM(VOTE),CANDIDATE FROM `result_candidate` ,`result_section` WHERE SECTION_ID = result_section.ID AND result_section.ELECTION = ?  GROUP BY CANDIDATE ORDER BY VOTE DESC;";


    $out = [];

    if ($stmt = $conn->prepare($sql)) {
        $stmt->bind_param("s", $election);


        $result = $stmt->execute();


        $result = $stmt->get_result();


        while ($row = $result->fetch_assoc()) {

            $out[] = $row;

        }
    }

    return $out;
}

function getSections($election)
{
    global $conn;
    $sql = "SELECT * FROM `result_section` WHERE ELECTION = ? ORDER BY P_2019_V DESC";


    $out = [];

    if ($stmt = $conn->prepare($sql)) {
        $stmt->bind_param("s", $election);


        $result = $stmt->execute();


        $result = $stmt->get_result();


        while ($row = $result->fetch_assoc()) {

            $out[] = $row;

        }
    }

    return $out;
}

function getDistricts()
{
    global $conn;
    $sql = "SELECT DISTRICT FROM `result_section` GROUP BY DISTRICT";


    $out = [];

    if ($stmt = $conn->prepare($sql)) {
//        $stmt->bind_param("s", $election);


        $result = $stmt->execute();


        $result = $stmt->get_result();


        while ($row = $result->fetch_assoc()) {

            $out[] = $row;

        }
    }

    return $out;
}

function updateSection($section, $postal, $gr, $sp, $district, $total)
{
    global $conn;
    $sql = "SELECT * FROM `result_section` WHERE ELECTION = '2019' AND NAME = ? AND POSTAL = ? ";


    $id = null;

    if ($stmt = $conn->prepare($sql)) {
        $postal = $postal ? 1 : 0;
        $stmt->bind_param("ss", $section, $postal);
        $result = $stmt->execute();
        $result = $stmt->get_result();
        while ($row = $result->fetch_assoc()) {
            $id = $row["ID"];
        }
    }

    if ($id == null) {
        $id = addResultSet($section, $total, 0, 0, 0, 0, 0, 0, "2019", $postal, $district);
    }
    update2019($id, $gr, $sp, $postal);

    return $id;


}

function update2019($id, $vote, $sp, $postal)
{
    $sql = "UPDATE `result_section` SET `P_2019_V` = ?,`P_2019SP_V` = ? WHERE `result_section`.`ID` = ?;";

    if ($postal)
        $sql = "UPDATE `result_section` SET `P_2019_VP` = ?,`P_2019SP_VP` = ? WHERE `result_section`.`ID` = ?;";
    global $conn;
    if ($stmt = $conn->prepare($sql)) {
        $stmt->bind_param("sss", $vote, $sp, $id);
        $result = $stmt->execute();
    }
}

function addResult($section, $candidate, $vote, $postal, $sectionID, $total,$room)
{
   // echo $candidate." ".$vote." ".$postal." ".$sectionID;
    addCandidate($sectionID, $candidate, $vote, getPercent($vote, $total),$room);
}

function addResultSet($name, $validV, $rejected, $totalPolled, $validV_P, $rejected_P, $totalPolled_P, $regElector, $year, $postal, $PD)
{
    echo $name . " " . $PD . " <br>";
    $validV = ($validV);
    $rejected = ($rejected);
    $totalPolled = ($totalPolled);
    $regElector = ($regElector);

    $postal = ($postal ? 1 : 0);
    $did = $name . "_" . $postal;

    $sql = "INSERT INTO `result_section` (`ID`, `NAME`, `VALID_V`, `REJECTED_V`, `TOTAL_POLLED`, `VALID_V_P`, `REJECTED_V_P`, `TOTAL_POLLED_P`, `R_ELECTORS`, `ELECTION`,`POSTAL`,`DISTRICT`,`DID`) 
                VALUES (NULL, ?, ?, ?, ?,?, ?, ?, ?, ?,?,?,?);";
    global $conn;
    if ($stmt = $conn->prepare($sql)) {
        $stmt->bind_param("ssssssssssss", $name, $validV, $rejected, $totalPolled, $validV_P, $rejected_P, $totalPolled_P, $regElector, $year, $postal, $PD, $did);

        $result = $stmt->execute();

        return $conn->insert_id;


    }


}

function addCandidate($id, $candidate, $vote, $percent,$room)
{

    $vote = ($vote);
    $percent = ($percent);

    $sql = "INSERT INTO `result_candidate` (`ID`, `CANDIDATE`, `VOTE`, `PERCENTAGE`, `SECTION_ID`,`ROOM`) VALUES (NULL, ?,?,?,?,?)
            ON DUPLICATE KEY UPDATE VOTE=? ,PERCENTAGE=? , ROOM=?;";
    global $conn;
    if ($stmt = $conn->prepare($sql)) {
        $stmt->bind_param("ssssssss", $candidate, $vote, $percent, $id,$room, $vote, $percent,$room);

        $result = $stmt->execute();


    }


}

function getExceptResult($year, $candidate)
{
    $sql = "SELECT CANDIDATE,SUM(VOTE) as VOTE FROM result_candidate  WHERE SECTION_ID IN (  SELECT ID  FROM `result_section` WHERE `ELECTION` = ? AND DID NOT IN (SELECT DID  FROM `result_section` WHERE `ELECTION` = '2019' ORDER BY `ID` ASC  ))  AND CANDIDATE=? GROUP BY CANDIDATE;";
    global $conn;
    $out = [];
    if ($stmt = $conn->prepare($sql)) {
        $stmt->bind_param("ss", $year, $candidate);

        $result = $stmt->execute();
        $result = $stmt->get_result();

        while ($row = $result->fetch_assoc()) {

            $out[] = $row;

        }
    }

    return $out;
}
function getCurrentVotedResult($year, $candidate)
{
    $sql = "SELECT CANDIDATE,SUM(VOTE) as VOTE,AVG(PERCENTAGE) as PERCENTAGE  FROM result_candidate  WHERE SECTION_ID IN (  SELECT ID  FROM `result_section` WHERE `ELECTION` = ? AND DID  IN (SELECT DID  FROM `result_section` WHERE `ELECTION` = '2019' ORDER BY `ID` ASC  ))  AND CANDIDATE=? GROUP BY CANDIDATE;";
    global $conn;
    $out = [];
    if ($stmt = $conn->prepare($sql)) {
        $stmt->bind_param("ss", $year, $candidate);

        $result = $stmt->execute();
        $result = $stmt->get_result();

        while ($row = $result->fetch_assoc()) {

            $out[] = $row;

        }
    }

    return $out;
}

function getCurrentResult($year, $candidate)
{
    $sql = "SELECT CANDIDATE,SUM(VOTE) as VOTE,AVG(PERCENTAGE) as PERCENTAGE FROM result_candidate  WHERE SECTION_ID IN (  SELECT ID  FROM `result_section` WHERE `ELECTION` = ? ) AND CANDIDATE=? GROUP BY CANDIDATE";
    global $conn;
    $out = [];
    if ($stmt = $conn->prepare($sql)) {
        $stmt->bind_param("ss", $year, $candidate);

        $result = $stmt->execute();

        $result = $stmt->get_result();

        while ($row = $result->fetch_assoc()) {

            $out[] = $row;

        }
    }

    return $out;
}

function getCurrentResultOld($year)
{
    $sql = "SELECT SUM(VOTE) as VOTE,AVG(PERCENTAGE) as PERCENTAGE FROM result_candidate  WHERE SECTION_ID IN (  SELECT ID  FROM `result_section` WHERE `ELECTION` = ? ) ";
    global $conn;
    $out = [];
    if ($stmt = $conn->prepare($sql)) {
        $stmt->bind_param("s", $year);

        $result = $stmt->execute();

        $result = $stmt->get_result();

        while ($row = $result->fetch_assoc()) {

            $out[] = $row;

        }
    }

    return $out;
}

function getCurrentResultForYear($year, $section)
{
    $sql = "SELECT CANDIDATE, SUM(VOTE) as VOTE FROM result_candidate  WHERE SECTION_ID IN (  SELECT ID  FROM `result_section` WHERE `ELECTION` = ?  AND result_section.NAME = ?) GROUP BY CANDIDATE ";
    global $conn;
    $out = [];
    if ($stmt = $conn->prepare($sql)) {
        $stmt->bind_param("ss", $year, $section);

        $result = $stmt->execute();
        $result = $stmt->get_result();

        while ($row = $result->fetch_assoc()) {

            $out[] = $row;

        }
    }

    return $out;
}

function getCurrentRestultTotal($year)
{
    $sql = "SELECT CANDIDATE,SUM(VOTE) as VOTE FROM result_candidate  WHERE SECTION_ID IN (  SELECT ID  FROM `result_section` WHERE `ELECTION` = ? ) GROUP BY CANDIDATE; ";
    global $conn;
    $out = [];
    if ($stmt = $conn->prepare($sql)) {
        $stmt->bind_param("s", $year);

        $result = $stmt->execute();
        $result = $stmt->get_result();

        while ($row = $result->fetch_assoc()) {

            $out = $row;

        }
    }

    return $out;
}

function getCurrentRestultTotalFull($year)
{
    $sql = "SELECT SUM(VOTE) as VOTE FROM result_candidate  WHERE SECTION_ID IN (  SELECT ID  FROM `result_section` WHERE `ELECTION` = ? ) ; ";
    global $conn;
    $out = [];
    if ($stmt = $conn->prepare($sql)) {
        $stmt->bind_param("s", $year);

        $result = $stmt->execute();
        $result = $stmt->get_result();

        while ($row = $result->fetch_assoc()) {

            $out = $row;

        }
    }

    return $out;
}


function getPercent($vote, $total)
{
    $n = ($vote / $total) * 100;
    $n = number_format($n, 2);
    return $n;
}

function getSectionHistory($section, $election, $candidatesArray)
{
    $cstr = json_encode($candidatesArray);
    $cstr = str_replace("[", "", $cstr);
    $cstr = str_replace("]", "", $cstr);
    $sql = 'SELECT `result_section`.ELECTION,result_section.POSTAL, result_candidate.VOTE, result_candidate.PERCENTAGE FROM `result_section`,`result_candidate` WHERE result_candidate.SECTION_ID = result_section.ID and result_candidate.CANDIDATE IN (' . $cstr . ') AND result_section.NAME=? AND result_section.ELECTION = ? ';
    global $conn;
    $out = null;
    if ($stmt = $conn->prepare($sql)) {
        $stmt->bind_param("ss", $section, $election);

        $result = $stmt->execute();
        $result = $stmt->get_result();

        while ($row = $result->fetch_assoc()) {

            $out = $row;

        }
    }

    return $out;
}

function getSupportHistory( $election, $candidatesArray)
{
    $cstr = json_encode($candidatesArray);
    $cstr = str_replace("[", "", $cstr);
    $cstr = str_replace("]", "", $cstr);
    $sql = 'SELECT `result_section`.ELECTION,result_section.POSTAL, result_candidate.VOTE, result_candidate.PERCENTAGE FROM `result_section`,`result_candidate` WHERE result_candidate.SECTION_ID = result_section.ID and result_candidate.CANDIDATE IN (' . $cstr . ') AND  result_section.ELECTION = ? ';
    global $conn;
    $out = null;
    if ($stmt = $conn->prepare($sql)) {
        $stmt->bind_param("s", $election);

        $result = $stmt->execute();
        $result = $stmt->get_result();

        while ($row = $result->fetch_assoc()) {

            $out = $row;

        }
    }

    return $out;
}

function getSectionHistoryExcept($section, $election, $candidatesArray)
{
    $cstr = json_encode($candidatesArray);
    $cstr = str_replace("[", "", $cstr);
    $cstr = str_replace("]", "", $cstr);
    $sql = 'SELECT `result_section`.ELECTION,result_section.POSTAL, sum(result_candidate.VOTE) as VOTE, result_candidate.PERCENTAGE FROM `result_section`,`result_candidate` WHERE result_candidate.SECTION_ID = result_section.ID and result_candidate.CANDIDATE NOT IN (' . $cstr . ') AND result_section.NAME=? AND result_section.ELECTION = ? ';
    global $conn;
    $out = [];
    if ($stmt = $conn->prepare($sql)) {
        $stmt->bind_param("ss", $section, $election);

        $result = $stmt->execute();
        $result = $stmt->get_result();

        while ($row = $result->fetch_assoc()) {

            $out = $row;

        }
    }

    return $out;
}

function getResultName($name, $array)
{
    for ($x = 0; $x < count($array); $x++) {
        if ($array[$x]["CANDIDATE"] == $name)
            return $array[$x]["VOTE"];
    }

    return 0;
}

function getDistrictSummery($election)
{
    $sql = "SELECT DISTRICT,SUM(P_2019_V) as G_VOTE,SUM(P_2019_VP) as G_VOTE_P,SUM(P_2019SP_V) as SP_VOTE,SUM(P_2019SP_VP) as SP_VOTE_P FROM `result_section` WHERE election = ? GROUP by DISTRICT";
    global $conn;
    $out = [];
    if ($stmt = $conn->prepare($sql)) {
        $stmt->bind_param("s", $election);

        $result = $stmt->execute();
        $result = $stmt->get_result();

        while ($row = $result->fetch_assoc()) {
            if (($row["G_VOTE"] + $row["G_VOTE_P"]) > ($row['SP_VOTE'] + $row['SP_VOTE_P']))
                $row['LEAD'] = "GR";
            elseif (($row["G_VOTE"] + $row["G_VOTE_P"]) < ($row['SP_VOTE'] + $row['SP_VOTE_P']))
                $row['LEAD'] = "SP";
            else
                $row['LEAD'] = "NONE";
            $out[] = $row;

        }
    }

    return $out;
}

function formatMapName($name){
    $name = str_replace(" ","",$name);
    $name = strtolower($name);
if(substr($name,-1)=="s")
    $name =substr($name,0,-1);

if($name=="kaluthara")
    $name = "kalutara";
    return $name;
}