<?php

include_once 'common_ui.go';
$title = $title . " - Dashboard";
include_once 'views/header' . $URL_Extension;

$y2015E_Mahinda = getExceptResult("2015", "Mahinda Rajapaksa");
$y2015_Mahinda = getCurrentResult("2015", "Mahinda Rajapaksa");
$current_G = getCurrentResult("2019", $name_GR);

$y2015E_MP = getExceptResult("2015", "Maithripala Sirisena");
$y2015_MP = getCurrentResult("2015", "Maithripala Sirisena");
$current_SP = getCurrentResult("2019", $name_SP);

if (count($current_G) == 0) {
    $current_G[] = array("CANDIDATE" => "", "VOTE" => 0);
}
if (count($current_SP) == 0) {
    $current_SP[] = array("CANDIDATE" => "", "VOTE" => 0);
}

$gapG = ($y2015E_Mahinda[0]["VOTE"] + $current_G[0]["VOTE"]) - $y2015_Mahinda[0]["VOTE"];
$gapSP = ($y2015E_MP[0]["VOTE"] + $current_SP[0]["VOTE"]) - $y2015_MP[0]["VOTE"];

$y2018_SLPP = null;
$y2018_UNP = null;
?>
<div class="row">
    <div id="app" class="col-sm-6">
        <h2></h2>


        <div class="card card-body col-sm-12">
            2015 total for <h3>Mahinda Rajapaksa</h3><br>
            <?php echo number_format($y2015_Mahinda[0]["VOTE"]); ?>
        </div>
        <div class="card  col-sm-12 card-body">
            2018 total for SLPP<br>
            <?php echo number_format($y2018_SLPP[0]["VOTE"]); ?>
        </div>
        <div class="card card-body col-sm-12">
            2019 forecast for<h3>Gotabaya Rajapaksa</h3><br>
           <div class="row" style="margin: 5px">
            <?php
            if ($gapG > 0) {
                echo number_format($y2015E_Mahinda[0]["VOTE"] + $current_G[0]["VOTE"])." <span class='badge badge-success'>" . $gapG . "<i class=\"fas fa-arrow-up\"></i></span>";
            } elseif ($gapG < 0) {
                echo number_format($y2015E_Mahinda[0]["VOTE"] + $current_G[0]["VOTE"])." <span class='badge badge-danger'>" . $gapG . "<i class=\"fas fa-arrow-down\"></i></span>";
            }else{
                echo "Waiting for result";
            }

            ?>
           </div>
        </div>
        <div class="card card-body col-sm-12">
            2019 total as of now <h3>Gotabaya Rajapaksa</h3><br>

            <?php echo number_format($current_G[0]["VOTE"]); ?>
        </div>

    </div>
    <div id="app" class="col-sm-6">
        <h2></h2>


        <div class="card card-body col-sm-12">
            2015 total for <h3>Maithripala Sirisena</h3><br>
            <?php echo number_format($y2015_MP[0]["VOTE"]); ?>
        </div>
        <div class="card card-body col-sm-12">
            2018 total for UNP<br>
            <?php echo number_format($y2018_UNP[0]["VOTE"]); ?>
        </div>
        <div class="card card-body col-sm-12">
            2019 forecast for <h3>Sajith Premadasa</h3><br>

            <div class="row"style="margin: 5px" >
                <?php
            if ($gapSP > 0) {
                echo number_format($y2015E_MP[0]["VOTE"] + $current_SP[0]["VOTE"])." <span class='badge badge-success'>" . $gapSP . "<i class=\"fas fa-arrow-up\"></i></span>";
            } elseif ($gapSP < 0) {
                echo number_format($y2015E_MP[0]["VOTE"] + $current_SP[0]["VOTE"])." <span class='badge badge-danger'>" . $gapSP . "<i class=\"fas fa-arrow-down\"></i></span>";
            }else{
                echo "Waiting for result";
            }
            ?>
            </div>
        </div>
        <div class="card card-body col-sm-12">
            2019 total as of now  <h3>Sajith Premadasa</h3><br>

            <?php echo number_format($current_SP[0]["VOTE"]); ?>
        </div>

    </div>
</div>
<?php

include_once 'views/footer' . $URL_Extension;
include 'info.modal.go';

?>
<script src="static/js/jquery.dataTables.min.js"></script>
<link href="static/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="static/css/datatable.bootstrap.css" rel="stylesheet">

<script>
    var tempate = 1;
    var app = new Vue({
        el: '#app',
        data: {
            item: []
        },
        methods: {
            loadData: function () {
                var self = this;
                $.getJSON(apibase + "api/result-table.go", function (data) {

                    if (data.code === 2000) {
                        // debugger;
                        self.item = data.data
                        buildTable();

                    } else {
                        handleError(data)
                    }

                });
            },

            getAgo: function (time) {
                // debugger;
                console.log(time);

                return moment(time + " +5:30", "YYYY-MM-DD HH:mm:ss Z").fromNow();
            },
            loadItem: function (id) {
                loadItem(id);
            }
        }, mounted: function () {
            //this.loadData();

            this.loadData();

        }
    });

    function buildTable() {
        $(document).ready(function () {
            $('#myTable').DataTable();
        });
    }
</script>

