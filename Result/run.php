<?php
include "db.conf.go";
$last = "";

function loadFile($name)
{
    global $last;
    $out = [];


    $file = file_get_contents($name);

    $table = get_string_between($file, "<table", "</table");

    //$tables = explode("CANDIDATE",$table);

    //  for($x=0;$x<count($tables);$x++){

    $d = processSection($table, 0, $last);

    $out[] = $d["data"];

    while ($d['hasNext']) {
        $d = processSection($table, $d['lastIndex'] + 1, $d["data"]['district']);

        $out[] = $d["data"];

    }
    // }

    return $out;
}

function processSection($body, $start, $lastDistrict)
{
    global $last;
    $out = [];
    $out['candidates'] = [];

    $lines = explode("</tr>", $body);

    $PD = get_string_between($lines[0], "#District", "</td");
    if (strlen($PD) > 2 && ($start == 0) ) {
        if (str_replace('">', "", $PD) != $PD)
            $PD = get_string_between($PD, '">', "</");
        if (str_replace('<span', "", $PD) != $PD)
            $PD = get_string_between(">" . $PD, '>', "<");

        $PD = trim($PD);
        $PD = str_replace(" District", "", $PD);
        if (strlen($PD) > 3)
            $last = $PD;


        $start = $start + 1;
    }
    $out['PD'] = $last;


    $td = explode("</td>", $lines[$start]);
    $district = get_string_between($td[0], "<font", "</font") . "<";
    $district = get_string_between($district, ">", "<");
    $out['district'] = $district;
    $out['postal'] = false;

    if (substr($district, 0, strlen("Postal")) == "Postal") {
        $out['postal'] = true;
        $out['district'] = $last;
    }

    $finish = false;
    for ($x = $start + 1; $x < count($lines); $x++) {

        $line = $lines[$x];
        $candidate = get_string_between($line, "<font", "</font") . "<";
        $candidate = get_string_between($candidate, ">", "<");
        $candidate = trim(str_replace("\n", "", $candidate));


        if ($candidate == "") {
            $finish = true;
            continue;
        }

        $td = explode("</td>", $line);
        $vote = get_string_between($td[1] . "<", ">", "<");
        $percent = get_string_between($td[2] . "<", ">", "<");

        if ($finish) {
            $out[$candidate] = array("vote" => $vote, "percentage" => $percent);
        } else {
            if (substr($candidate, 0, strlen("CANDIDATE")) != "CANDIDATE")
                $out['candidates'][] = array("name" => $candidate, "vote" => $vote, "percentage" => $percent);

            // echo $candidate . " " . $vote . " " . $percent . "<br>";
        }

        if (substr($candidate, 0, strlen("Regis")) == "Regis") {
            //$last = $district;
            break;
        }
    }

    $hasNext = false;
    if (count($lines) > $x + 3)
        $hasNext = true;
    $arr = array("data" => $out, "lastIndex" => $x, "hasNext" => $hasNext);
    return $arr;
}


$files = array();
$data = [];
foreach ((glob("Results\DPresidentialElections2015-converted_files/sheet*.htm")) as $file) {
    echo "---------------------------------------<br>";
    echo $file . "<br>";
    $c = loadFile($file);


    echo($last . ", " . $c[0]['district'] . ", " . $c[0]['PD'] . "<br>");
    if (count($c) > 1)
        echo($last . " " . $c[1]['district'] . " " . $c[1]['PD'] . "<br>");

    $data = array_merge($c, $data);

}
for ($x = 0; $x < count($data); $x++) {
    $c = $data[$x];

    if (count($c['candidates']) > 5) {
        $id = addResultSet($c['district'], $c['Valid Votes']['vote'], $c['Rejected Votes']['vote'], $c['Total Polled']['vote'], $c['Valid Votes']['percentage'], $c['Rejected Votes']['percentage'], $c['Total Polled']['percentage'], $c['Regis.Electors']['vote'], "2015", $c['postal'], $c['PD']);

        for ($cc = 0; $cc < count($c['candidates']); $cc++) {
            addCandidate($id, $c['candidates'][$cc]['name'], $c['candidates'][$cc]['vote'], $c['candidates'][$cc]['percentage']);
        }
    }
}

echo json_encode($data);

function elementToArray($contents, $tag)
{
    $arr = array();

    if ($contents == null || $contents == '' || strlen($contents) < 20)
        return $arr;
    try {

        $DOM = new DOMDocument;
        $DOM->loadHTML($contents);

        $items = $DOM->getElementsByTagName($tag);

        foreach ($items as $node) {
            array_push($arr, $node->nodeValue);
        }
    } catch (Exception $e) {
    }


    return $arr;

}

function getTableValue($str, $td1)
{
    $a = trim(get_string_between($str, $td1, "</tr>"));
    $a = trim(get_string_between($a, 'class="cls_item_light">', "</td>"));
    return $a;
}


function get_string_between($string, $start, $end)
{
    $string = " " . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return "";
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}

function addResultSet($name, $validV, $rejected, $totalPolled, $validV_P, $rejected_P, $totalPolled_P, $regElector, $year, $postal, $PD)
{
    echo $name . " " . $PD . " <br>";
    $validV = toNum($validV);
    $rejected = toNum($rejected);
    $totalPolled = toNum($totalPolled);
    $regElector = toNum($regElector);

    $postal = ($postal ? 1 : 0);
    $did = $name."_".$postal;

    $sql = "INSERT INTO `result_section` (`ID`, `NAME`, `VALID_V`, `REJECTED_V`, `TOTAL_POLLED`, `VALID_V_P`, `REJECTED_V_P`, `TOTAL_POLLED_P`, `R_ELECTORS`, `ELECTION`,`POSTAL`,`DISTRICT`,`DID`) 
                VALUES (NULL, ?, ?, ?, ?,?, ?, ?, ?, ?,?,?,?);";
    global $conn;
    if ($stmt = $conn->prepare($sql)) {
        $stmt->bind_param("ssssssssssss", $name, $validV, $rejected, $totalPolled, $validV_P, $rejected_P, $totalPolled_P, $regElector, $year, $postal, $PD,$did);

        $result = $stmt->execute();

        return $conn->insert_id;


    }


}

function addCandidate($id, $candidate, $vote, $percent)
{

    $vote = toNum($vote);
    $percent = toNum($percent);

    $sql = "INSERT INTO `result_candidate` (`ID`, `CANDIDATE`, `VOTE`, `PERCENTAGE`, `SECTION_ID`) VALUES (NULL, ?,?,?,?);";
    global $conn;
    if ($stmt = $conn->prepare($sql)) {
        $stmt->bind_param("ssss", $candidate, $vote, $percent, $id);

        $result = $stmt->execute();

    }


}

function toNum($num)
{
    $num = str_replace(",", "", $num);
    return $num;
}